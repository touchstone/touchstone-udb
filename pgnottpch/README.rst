===============
pgnottpch notes
===============

Overview
========

This is **NOT** a TPC-H.  Go to `<http://tpc.org/tpch/>`_ for a real TPC-H.

This is a set of scripts that perform some tasks that appear to be a TPC-H
benchmark but is absolutely not comparable to a TPC-H benchmark.

In order to use these scripts, the TPC-H **dbgen** and **qgen** programs must
be installed into the user's path.  The PostgreSQL postmaker must also have
**dbgen** in its path.

Query templates must also be available for PostgreSQL for **qgen** to work
properly.

Edit the provided *profile* file to make sure environment variables are set
appropriately for your system, which includes things like:

* the database name to use: **nottpch**
* test scale factor: 1

The `DSS_CONFIG` and `DSS_QUERY` environment variables must also be set in
*profile*, per dbgen and qgen instructions.
